c0,c1, c2, c3, c4, c5, c6, c7, c8, c9, c10 = var('c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10') # coefficients of linear combination; more can be added if needed
c = [c0,c1, c2, c3, c4, c5, c6, c7, c8, c9, c10]
k = 12 # Dimension of M_k; user can change it
eqn = [] # eqn will be the system of equation to solve to get a modular form as combination of basis elements
M = [] # M will consist of E_k and basis of M_k
M.append(CuspForms(1,12).0) # There could be any other modular form of weight k as well; user can choose desired one
for i in IntegerRange(ceil(k/6), floor(k/4)+1):
    M.append(eisenstein_series_qexp(4, normalization='constant')^(3*i-k/2)*eisenstein_series_qexp(6, normalization='constant')^(-2*i+k/2)) # This is our basis of M_k
    print('c_',i-ceil(k/6),'*','E_4^', (3*i-k/2),'*', ' E_6^', (-2*i+k/2))
for i in range(len(IntegerRange(ceil(k/6), floor(k/4)+1))):
    eqn1 = 0 # the right side of our equation for every coefficient of q^n
    for j in range(len(IntegerRange(ceil(k/6), floor(k/4)+1))):
        eqn1 = eqn1 + c[j]*M[j+1][i]
    eqn.append(M[0][i]==eqn1) 
solve(eqn, c[:len(IntegerRange(ceil(k/6), floor(k/4)+1))]) # Solving equation
