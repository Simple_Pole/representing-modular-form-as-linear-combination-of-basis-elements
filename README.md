# Representing modular form as linear combination of basis elements

Given code can represent given modular form as linear combination of basis elements (for modular forms for the full modular group). This can be used e.g.  to obtain interesting identities for sigma (sums of divisors) function.

The basis of $`M_k`$ (the space of modular forms of weight k for $`SL_2(\mathbb{Z})`$) is 
```math
\{E_{4}^{a}E_6^b \mid 4a+6b=k, \text{ with }a,b\geq0,\text{ } a,b\in\mathbb{Z}\}
```
The problem to obtain such identities is that multiplying symbolic expression with modular form will give an error (TypeError: unsupported operand parent(s) for *: 'Symbolic Ring' and 'Power Series Ring in q over Rational Field'). In this repository I represent my solution to this problem, working on choosen coefficients. 

Representing modular form as combination of Eisenstein Series can be useful to obtain identities between sigma funtion as well as between e.g. Ramanujan tau function and sigma function. They can be obtained using Cauchy product of two power series 
```math
\left(\sum_{n=1}^{\infty} a_nx^n\right)\left(\sum_{m=1}^{\infty} b_mx^m\right)=\sum_{k=2}^{\infty}\left(\sum_{l=1}^{k-1}a_lb_{k-l}\right)x^k
```

the binomial theorem and the formula 
```math
\left(\sum_{k=1}^{\infty}a_kx^k\right)^n=\sum_{k=2}^{\infty}\left(\sum_{\substack{0<r_1,\dots ,r_n<k \\ r_1+\dots +r_n=k}}a_{r_1}\dots a_{r_n}\right)x^k
```

Example:
Example - E_12 (Eisenstein series of weight 12) gave us that $`E_{12}=\frac{250}{691}E_6^2+\frac{441}{691}E_4^3`$. Thus we will calculate as an example the $`\sigma_{11}`$ as the combination of other sigma functions. We start by calculating q-expansion of $`E_6^2`$:
```math
E_6^2=\left(1-504\sum_{k=1}^{\infty}\sigma_5(k)q^k\right)^2=1-2 \cdot 504\sum_{k=1}^{\infty}\sigma_5(k)q^k+504^2\left(\sum_{k=1}^{\infty}\sigma_5(k)q^k\right)^2=
```
 
 
```math
=1-1008\sum_{k=1}^{\infty}\sigma_5(k)q^k+504^2\sum_{k=1}^{\infty}\left(\sum_{\substack{0<r_1,r_2<k \\ r_1+r_2=k}}\sigma_5(r_1)\sigma_5(r_2)\right)q^k=1-1008\sum_{k=1}^{\infty}\sigma_5(k)q^k+504^2\sum_{k=1}^{\infty}\left(\sum_{r=1}^{k-1}\sigma_5(r)\sigma_5(k-r)\right)q^k
```
Now $`E_{4}^{3}`$:
```math
E_4^3=\left(1+240\sum_{k=1}^{\infty}\sigma_3(k)q^k\right)^3=1+3\cdot240\sum_{k=1}^{\infty}\sigma_3(k)q^k+3\cdot240^2\left(\sum_{k=1}^{\infty}\sigma_3(k)q^k\right)^2+240^3\left(\sum_{k=1}^{\infty}\sigma_3(k)q^k\right)^3=
```
    
```math
=1+720\sum_{k=1}^{\infty}\sigma_3(k)q^k+3\cdot240^2\sum_{k=1}^{\infty}\left(\sum_{r=1}^{k-1}\sigma_3(r)\sigma_3(k-r)\right)q^k+240^3\sum_{k=1}^{\infty}\left(\sum_{\substack{0<r_1,r_2 ,r_3<k \\ r_1+r_2 +r_3=k}}\sigma_3(r_1)\sigma_3(r_2)\sigma_3(r_3)\right)q^k
```
and hence by comparing coefficients of $`q^{n}`$, multiplying by $`691`$ and dividing by $`5040`$ (which is gcd of left coefficient numbers) we get 
```math
13\sigma_{11}(n)=-50\sigma_5(n)+12600\sum_{r=1}^{n-1}\sigma_5(r)\sigma_5(n-r)+63\sigma_3(n)+15120\sum_{r=1}^{n-1}\sigma_3(r)\sigma_3(n-r)+1209600\sum_{\substack{0<r_1,r_2 ,r_3<k \\ r_1+r_2 +r_3=k}}\sigma_3(r_1)\sigma_3(r_2)\sigma_3(r_3).
```
